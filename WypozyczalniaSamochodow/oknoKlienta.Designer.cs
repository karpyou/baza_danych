﻿namespace WypozyczalniaSamochodow
{
    partial class oknoKlienta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(oknoKlienta));
            this.wyswietlaczDanych = new System.Windows.Forms.DataGridView();
            this.MojeWypozyczenia = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.buttonWyslijEmail = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.wyswietlaczDanych)).BeginInit();
            this.SuspendLayout();
            // 
            // wyswietlaczDanych
            // 
            this.wyswietlaczDanych.AllowUserToAddRows = false;
            this.wyswietlaczDanych.AllowUserToDeleteRows = false;
            this.wyswietlaczDanych.AllowUserToResizeColumns = false;
            this.wyswietlaczDanych.AllowUserToResizeRows = false;
            this.wyswietlaczDanych.BackgroundColor = System.Drawing.Color.White;
            this.wyswietlaczDanych.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.wyswietlaczDanych.Location = new System.Drawing.Point(255, 32);
            this.wyswietlaczDanych.Margin = new System.Windows.Forms.Padding(4);
            this.wyswietlaczDanych.Name = "wyswietlaczDanych";
            this.wyswietlaczDanych.ReadOnly = true;
            this.wyswietlaczDanych.Size = new System.Drawing.Size(688, 409);
            this.wyswietlaczDanych.TabIndex = 0;
            // 
            // MojeWypozyczenia
            // 
            this.MojeWypozyczenia.Location = new System.Drawing.Point(48, 79);
            this.MojeWypozyczenia.Margin = new System.Windows.Forms.Padding(4);
            this.MojeWypozyczenia.Name = "MojeWypozyczenia";
            this.MojeWypozyczenia.Size = new System.Drawing.Size(163, 42);
            this.MojeWypozyczenia.TabIndex = 1;
            this.MojeWypozyczenia.Text = "Moje wypożyczenia";
            this.MojeWypozyczenia.UseVisualStyleBackColor = true;
            this.MojeWypozyczenia.Click += new System.EventHandler(this.MojeWypozyczenia_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(48, 16);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(46, 17);
            this.label1.TabIndex = 2;
            this.label1.Text = "label1";
            // 
            // buttonWyslijEmail
            // 
            this.buttonWyslijEmail.Location = new System.Drawing.Point(48, 278);
            this.buttonWyslijEmail.Margin = new System.Windows.Forms.Padding(4);
            this.buttonWyslijEmail.Name = "buttonWyslijEmail";
            this.buttonWyslijEmail.Size = new System.Drawing.Size(163, 46);
            this.buttonWyslijEmail.TabIndex = 3;
            this.buttonWyslijEmail.Text = "Wyslij zapytanie";
            this.buttonWyslijEmail.UseVisualStyleBackColor = true;
            this.buttonWyslijEmail.Click += new System.EventHandler(this.buttonWyslijEmail_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(48, 46);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(83, 29);
            this.label2.TabIndex = 4;
            this.label2.Text = "Menu:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(48, 128);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(163, 42);
            this.button1.TabIndex = 5;
            this.button1.Text = "Sprawdź dostępne samochody";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(48, 176);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(163, 45);
            this.button2.TabIndex = 6;
            this.button2.Text = "Zarezerwuj samochód";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(48, 227);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(163, 44);
            this.button3.TabIndex = 7;
            this.button3.Text = "Polecenie raportowe";
            this.button3.UseVisualStyleBackColor = true;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(804, 462);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(163, 47);
            this.button4.TabIndex = 8;
            this.button4.Text = "Wyloguj";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // oknoKlienta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(979, 521);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.buttonWyslijEmail);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.MojeWypozyczenia);
            this.Controls.Add(this.wyswietlaczDanych);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "oknoKlienta";
            this.Text = "oknoKlienta";
            ((System.ComponentModel.ISupportInitialize)(this.wyswietlaczDanych)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView wyswietlaczDanych;
        private System.Windows.Forms.Button MojeWypozyczenia;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonWyslijEmail;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
    }
}