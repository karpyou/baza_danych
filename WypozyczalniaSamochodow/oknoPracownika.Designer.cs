﻿namespace WypozyczalniaSamochodow
{
    partial class oknoPracownika
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(oknoPracownika));
            this.wyswietlaczDanych = new System.Windows.Forms.DataGridView();
            this.buttonPokazKlientow = new System.Windows.Forms.Button();
            this.buttonDodajKlienta = new System.Windows.Forms.Button();
            this.buttonUsunKlienta = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.wyswietlaczDanych)).BeginInit();
            this.SuspendLayout();
            // 
            // wyswietlaczDanych
            // 
            this.wyswietlaczDanych.AllowUserToAddRows = false;
            this.wyswietlaczDanych.AllowUserToDeleteRows = false;
            this.wyswietlaczDanych.AllowUserToResizeRows = false;
            this.wyswietlaczDanych.BackgroundColor = System.Drawing.Color.White;
            this.wyswietlaczDanych.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.wyswietlaczDanych.Location = new System.Drawing.Point(246, 15);
            this.wyswietlaczDanych.Margin = new System.Windows.Forms.Padding(4);
            this.wyswietlaczDanych.Name = "wyswietlaczDanych";
            this.wyswietlaczDanych.ReadOnly = true;
            this.wyswietlaczDanych.Size = new System.Drawing.Size(733, 521);
            this.wyswietlaczDanych.TabIndex = 0;
            // 
            // buttonPokazKlientow
            // 
            this.buttonPokazKlientow.Location = new System.Drawing.Point(35, 66);
            this.buttonPokazKlientow.Margin = new System.Windows.Forms.Padding(4);
            this.buttonPokazKlientow.Name = "buttonPokazKlientow";
            this.buttonPokazKlientow.Size = new System.Drawing.Size(156, 32);
            this.buttonPokazKlientow.TabIndex = 1;
            this.buttonPokazKlientow.Text = "Pokaż klientów";
            this.buttonPokazKlientow.UseVisualStyleBackColor = true;
            this.buttonPokazKlientow.Click += new System.EventHandler(this.buttonPokazKlientow_Click);
            // 
            // buttonDodajKlienta
            // 
            this.buttonDodajKlienta.Location = new System.Drawing.Point(35, 106);
            this.buttonDodajKlienta.Margin = new System.Windows.Forms.Padding(4);
            this.buttonDodajKlienta.Name = "buttonDodajKlienta";
            this.buttonDodajKlienta.Size = new System.Drawing.Size(156, 33);
            this.buttonDodajKlienta.TabIndex = 2;
            this.buttonDodajKlienta.Text = "Dodaj klienta";
            this.buttonDodajKlienta.UseVisualStyleBackColor = true;
            this.buttonDodajKlienta.Click += new System.EventHandler(this.buttonDodajKlienta_Click);
            // 
            // buttonUsunKlienta
            // 
            this.buttonUsunKlienta.Location = new System.Drawing.Point(35, 146);
            this.buttonUsunKlienta.Name = "buttonUsunKlienta";
            this.buttonUsunKlienta.Size = new System.Drawing.Size(156, 33);
            this.buttonUsunKlienta.TabIndex = 3;
            this.buttonUsunKlienta.Text = "Usuń klienta";
            this.buttonUsunKlienta.UseVisualStyleBackColor = true;
            this.buttonUsunKlienta.Click += new System.EventHandler(this.buttonUsunKlienta_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.White;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(30, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(164, 29);
            this.label1.TabIndex = 4;
            this.label1.Text = "Menu klienta:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(35, 185);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(156, 37);
            this.button1.TabIndex = 5;
            this.button1.Text = "Zmodyfikuj klienta";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.buttonZmodyfikujKlienta_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.White;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(30, 292);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(181, 25);
            this.label2.TabIndex = 6;
            this.label2.Text = "Menu wypożyczeń:";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(35, 331);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(156, 45);
            this.button2.TabIndex = 7;
            this.button2.Text = "Wypożyczenia klienta";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(35, 382);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(156, 45);
            this.button3.TabIndex = 8;
            this.button3.Text = "Niesfinalizowane rezerwacje";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(35, 433);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(156, 42);
            this.button4.TabIndex = 9;
            this.button4.Text = "Wydanie samochodu";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(35, 481);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(156, 45);
            this.button5.TabIndex = 10;
            this.button5.Text = "Zwrot samochodu";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(827, 560);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(156, 41);
            this.button6.TabIndex = 11;
            this.button6.Text = "Wyloguj";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(35, 532);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(156, 46);
            this.button7.TabIndex = 12;
            this.button7.Text = "Polecenie raportowe";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // oknoPracownika
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(995, 613);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonUsunKlienta);
            this.Controls.Add(this.buttonDodajKlienta);
            this.Controls.Add(this.buttonPokazKlientow);
            this.Controls.Add(this.wyswietlaczDanych);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "oknoPracownika";
            this.Text = "oknoPracownika";
            ((System.ComponentModel.ISupportInitialize)(this.wyswietlaczDanych)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView wyswietlaczDanych;
        private System.Windows.Forms.Button buttonPokazKlientow;
        private System.Windows.Forms.Button buttonDodajKlienta;
        private System.Windows.Forms.Button buttonUsunKlienta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
    }
}