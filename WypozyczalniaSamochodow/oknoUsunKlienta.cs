﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WypozyczalniaSamochodow
{
    public partial class oknoUsunKlienta : Form
    {
        string _connectionString = "Data Source=DESKTOP-MOGMF2U\\SQLEXPRESS;Initial Catalog=WypozyczalniaSamochodow;Integrated Security=True";
        public oknoUsunKlienta()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int idKlienta = int.Parse(textBox1.Text);

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                string polecenie1 = "DELETE FROM KLIENCI WHERE KlientID = " + idKlienta;
                var usuwacz = new Sprawdzacz(idKlienta);

                conn.Open();
                if (usuwacz.usunKlienta()) 
                {
                    SqlCommand cmd1 = new SqlCommand(polecenie1, conn);
                    cmd1.ExecuteScalar();
                    MessageBox.Show("Usunięto klienta !");
                    this.Close();
                }
                
                else
                    MessageBox.Show("Nie ma takiego klienta!!!");
                


                conn.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
