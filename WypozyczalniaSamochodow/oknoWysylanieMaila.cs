﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WypozyczalniaSamochodow
{
    public partial class oknoWysylanieMaila : Form
    {
        private int _idKlienta;
        private string _Imie;
        private string _nazwisko;

        public oknoWysylanieMaila(int idKlienta,string Imie, string Nazwisko)
        {
            InitializeComponent();
            _idKlienta = idKlienta;
            _Imie = Imie;
            _nazwisko = Nazwisko;
        }

        private void buttonWyslij_Click(object sender, EventArgs e)
        {
            string tresc = BoxTresc.Text + "\n\nWysłano od: " + _idKlienta + " " + _Imie + " " + _nazwisko;
            obslugaEmail wyslij = new obslugaEmail(BoxTemat.Text, tresc);
            try
            {
                wyslij.email_send();
                MessageBox.Show("Wyslano");
                this.Close();
            }
            catch
            {
                //jakis wyjatek
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
