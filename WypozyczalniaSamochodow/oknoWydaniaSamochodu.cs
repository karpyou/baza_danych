﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WypozyczalniaSamochodow
{
    public partial class oknoWydaniaSamochodu : Form
    {
        string _connectionString = "Data Source=DESKTOP-MOGMF2U\\SQLEXPRESS;Initial Catalog=WypozyczalniaSamochodow;Integrated Security=True";
        public oknoWydaniaSamochodu()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Sprawdzacz sprawdz = new Sprawdzacz(int.Parse(textBox1.Text));


            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                string polecenie1 = "UPDATE WYPOZYCZENIE SET PracWypozycajacyID = " + int.Parse(textBox2.Text) + "WHERE WypozyczenieID =" + int.Parse(textBox1.Text);

                 conn.Open();
                if (sprawdz.czyIstniejeWyp())
                {
                    SqlCommand cmd1 = new SqlCommand(polecenie1, conn);
                    cmd1.ExecuteScalar();
                    MessageBox.Show("Wypożyczono samochód");
                    this.Close();
                }

                else
                    MessageBox.Show("Nie istnieje wypożyczenie o takim ID !!!");



                conn.Close();
            }


        }
    }
}
