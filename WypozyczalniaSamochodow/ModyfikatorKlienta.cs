﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace WypozyczalniaSamochodow
{
    class ModyfikatorKlienta
    {
        private int idKlienta;
        private string polecenie1;
        private string _connectionString = "Data Source=DESKTOP-MOGMF2U\\SQLEXPRESS;Initial Catalog=WypozyczalniaSamochodow;Integrated Security=True";

        public ModyfikatorKlienta(int id, string pol)
        {
            idKlienta = id;
            polecenie1 = pol;
        }

        public bool modyfikuj()
        {
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                var sprawdz = new Sprawdzacz(idKlienta);

                conn.Open();
                if (sprawdz.usunKlienta())
                {
                    SqlCommand cmd1 = new SqlCommand(polecenie1, conn);
                    cmd1.ExecuteScalar();
                    conn.Close();
                    return true;
                }

                else
                {
                    conn.Close();
                    return false;
                }
             
            }
        }
    }
}
