﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WypozyczalniaSamochodow
{
    public partial class oknoDodajKlienta : Form
    {
        string _connectionString = "Data Source=DESKTOP-MOGMF2U\\SQLEXPRESS;Initial Catalog=WypozyczalniaSamochodow;Integrated Security=True";
        public oknoDodajKlienta()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            string Imie = textBox1.Text;
            string Nazwisko = textBox2.Text;
            string Ulica = textBox3.Text;
            string NrDomu = textBox4.Text;
            string NrMieszkania = textBox5.Text;
            string KodPocztowy = textBox6.Text;
            string Miasto = textBox7.Text;
            string Telefon = textBox8.Text;
            string Login = textBox9.Text;
            string Haslo = textBox10.Text;
            string Pesel = textBox11.Text;

            string polecenie = "INSERT INTO KLIENCI ([Imie], [Nazwisko], [Ulica], [NumerDomu], [NumerMieszkania], [KodPocztowy], [Miasto], [Telefon], [Login], [Haslo], [Pesel]) VALUES('"+Imie+"', '"+Nazwisko+"', '"+Ulica+"', "+NrDomu.ToString()+",'"+NrMieszkania+"','"+KodPocztowy+"','"+Miasto+"', '"+Telefon+"', '" + Login + "', '" + Haslo + "', '" + Pesel + "')";
         

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                
                SqlCommand cmd = new SqlCommand(polecenie, conn);
                try
                {
                    cmd.ExecuteScalar();
                    MessageBox.Show("Dodano uzytkownika !");
                    this.Close();
                }
                catch(SqlException)
                {
                    MessageBox.Show("Niepoprawne dane!!!");
                }

                ;

                conn.Close();
            }
           }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
