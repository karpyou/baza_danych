//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WypozyczalniaSamochodow
{
    using System;
    using System.Collections.Generic;
    
    public partial class OBSLUGA
    {
        public Nullable<int> PracownikID { get; set; }
        public int SamochodID { get; set; }
        public int OperacjaID { get; set; }
        public System.DateTime DataRozpoczecia { get; set; }
        public System.DateTime DataZakonczenia { get; set; }
        public string Szczegoly { get; set; }
    
        public virtual OPERACJE OPERACJE { get; set; }
        public virtual PRACOWNICY PRACOWNICY { get; set; }
        public virtual SAMOCHODY SAMOCHODY { get; set; }
    }
}
