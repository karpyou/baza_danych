﻿namespace WypozyczalniaSamochodow
{
    partial class oknoWysylanieMaila
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BoxTresc = new System.Windows.Forms.TextBox();
            this.buttonWyslij = new System.Windows.Forms.Button();
            this.BoxTemat = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BoxTresc
            // 
            this.BoxTresc.Location = new System.Drawing.Point(16, 93);
            this.BoxTresc.Margin = new System.Windows.Forms.Padding(4);
            this.BoxTresc.Multiline = true;
            this.BoxTresc.Name = "BoxTresc";
            this.BoxTresc.Size = new System.Drawing.Size(608, 247);
            this.BoxTresc.TabIndex = 0;
            // 
            // buttonWyslij
            // 
            this.buttonWyslij.Location = new System.Drawing.Point(213, 350);
            this.buttonWyslij.Margin = new System.Windows.Forms.Padding(4);
            this.buttonWyslij.Name = "buttonWyslij";
            this.buttonWyslij.Size = new System.Drawing.Size(211, 58);
            this.buttonWyslij.TabIndex = 1;
            this.buttonWyslij.Text = "Wyślij";
            this.buttonWyslij.UseVisualStyleBackColor = true;
            this.buttonWyslij.Click += new System.EventHandler(this.buttonWyslij_Click);
            // 
            // BoxTemat
            // 
            this.BoxTemat.Location = new System.Drawing.Point(102, 13);
            this.BoxTemat.Margin = new System.Windows.Forms.Padding(4);
            this.BoxTemat.Name = "BoxTemat";
            this.BoxTemat.Size = new System.Drawing.Size(522, 22);
            this.BoxTemat.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label1.Location = new System.Drawing.Point(13, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 20);
            this.label1.TabIndex = 3;
            this.label1.Text = "Temat:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label2.Location = new System.Drawing.Point(13, 63);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(102, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "Treść maila:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(490, 423);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(114, 28);
            this.button1.TabIndex = 5;
            this.button1.Text = "Zamknij okno";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // oknoWysylanieMaila
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(641, 463);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BoxTemat);
            this.Controls.Add(this.buttonWyslij);
            this.Controls.Add(this.BoxTresc);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "oknoWysylanieMaila";
            this.Text = "oknoWysylanieMaila";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox BoxTresc;
        private System.Windows.Forms.Button buttonWyslij;
        private System.Windows.Forms.TextBox BoxTemat;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
    }
}