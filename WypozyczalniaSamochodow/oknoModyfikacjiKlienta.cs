﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WypozyczalniaSamochodow
{
    public partial class oknoModyfikacjiKlienta : Form
    {
        public oknoModyfikacjiKlienta()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            int idKlienta = int.Parse(textBox2.Text);
            string parametr = comboBox1.Text;
            string wartosc1;
            string polecenie1 = null;

            if (parametr == "NumerDomu" || parametr == "NumerMieszkania")
            {
                try
                {
                    int wartosc = int.Parse(textBox1.Text);
                    polecenie1 = "UPDATE KLIENCI SET " + comboBox1.Text + "=" + wartosc + " WHERE KlientID=" + idKlienta;
                    ModyfikatorKlienta mod = new ModyfikatorKlienta(idKlienta, polecenie1);
                    if (mod.modyfikuj())
                        MessageBox.Show("Uaktualniono klienta !");
                    else
                        MessageBox.Show("Nie ma takiego klienta!!!");
                    this.Close();
                }
                catch (FormatException)
                {
                    MessageBox.Show("Blędna wartość !");
                }
            }
            else
            {
                wartosc1 = textBox1.Text;
                polecenie1 = "UPDATE KLIENCI SET " + comboBox1.Text + "='" + wartosc1 + "' WHERE KlientID=" + idKlienta;
                ModyfikatorKlienta mod = new ModyfikatorKlienta(idKlienta, polecenie1);
                if (mod.modyfikuj())
                    MessageBox.Show("Uaktualniono klienta !");
                else
                    MessageBox.Show("Nie ma takiego klienta!!!");
                this.Close();
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
