//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace WypozyczalniaSamochodow
{
    using System;
    using System.Collections.Generic;
    
    public partial class TYP
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TYP()
        {
            this.SAMOCHODY = new HashSet<SAMOCHODY>();
        }
    
        public int TypID { get; set; }
        public string Marka { get; set; }
        public string Model { get; set; }
        public string TypNadwozia { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<SAMOCHODY> SAMOCHODY { get; set; }
    }
}
