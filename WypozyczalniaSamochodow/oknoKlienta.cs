﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WypozyczalniaSamochodow
{
    public partial class oknoKlienta : Form
    {
        private string _connectionString = "Data Source=DESKTOP-MOGMF2U\\SQLEXPRESS;Initial Catalog=WypozyczalniaSamochodow;Integrated Security=True";
        private string _login;
        private int _idKlienta;
        private string _imie;
        private string _nazwisko;
        private string _odKiedy;
        private string _doKiedy;

        public oknoKlienta(string odKiedy, string doKiedy, string login)
        {
            InitializeComponent();
            _odKiedy = odKiedy;
            _doKiedy = doKiedy;
            _login = login;
            // Musimy po loginie znalesc id, imie i nazwisko
            var Baza = new WypozyczalniaSamochodowEntities3();
            foreach (var klient in Baza.KLIENCI)
            {
                if (klient.Login == _login)
                {
                    _idKlienta = klient.KlientID;
                    _imie = klient.Imie;
                    _nazwisko = klient.Nazwisko;

                }
            }
            label1.Text = "Zalogowany jako: " + _imie + " " + _nazwisko;
        }

        public oknoKlienta(string login)
        {
            InitializeComponent();
            _login = login;
            // Musimy po loginie znalesc id, imie i nazwisko
            var Baza = new WypozyczalniaSamochodowEntities3();
            foreach (var klient in Baza.KLIENCI)
            {
                if (klient.Login == _login)
                {
                    _idKlienta = klient.KlientID;
                    _imie = klient.Imie;
                    _nazwisko = klient.Nazwisko;

                }
            }
            label1.Text = "Zalogowany jako: " + _imie + " " + _nazwisko;
        }

        public void MojeWypozyczenia_Click(object sender, EventArgs e)
        {
            string sql1 = "SELECT * FROM WYPOZYCZENIE WHERE KlientID = "+_idKlienta;
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlDataAdapter dataadapter = new SqlDataAdapter(sql1, connection);
            DataSet ds = new DataSet();
            connection.Open();
            dataadapter.Fill(ds, "KLIENCI");
            connection.Close();
            wyswietlaczDanych.DataSource = ds;
            wyswietlaczDanych.DataMember = "KLIENCI";

        }

        private void buttonWyslijEmail_Click(object sender, EventArgs e)
        {
            // Przesylamy id klienta, imie i nazwisko, zeby moc sobie sprawdzic numer telefonu zeby odpowiedziec
            oknoWysylanieMaila okienko = new oknoWysylanieMaila(_idKlienta,_imie,_nazwisko);
            okienko.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            oknoWyboruDaty okienko = new oknoWyboruDaty(_login);
            okienko.Show();
            this.Close();
        }

        public void wyswietl_pojazdy()
        {
            this.Show();
            string sql = "SELECT Marka, Model, TypNadwozia, NrRejestracyjny, PojSilnika, PojBagaznika, PojOsob FROM WYPOZYCZENIE e JOIN SAMOCHODY d ON e.SamochodID = d.SamochodID JOIN TYP f ON d.SamochodID = f.TypID WHERE (OdKiedy > '" + _odKiedy + "' and  OdKiedy > '" + _doKiedy + "') or (DoKiedy < '" + _odKiedy + "' and DoKiedy < '" + _doKiedy + "')";
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlDataAdapter dataadapter = new SqlDataAdapter(sql, connection);
            DataSet ds = new DataSet();
            connection.Open();
            dataadapter.Fill(ds, "WYPOZYCZENIE");
            connection.Close();
            wyswietlaczDanych.DataSource = ds;
            wyswietlaczDanych.DataMember = "WYPOZYCZENIE";

        }

        private void button2_Click(object sender, EventArgs e)
        {
            oknoRezerwacjiSamochodu okienko = new oknoRezerwacjiSamochodu(_login);
            okienko.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}