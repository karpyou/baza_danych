﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WypozyczalniaSamochodow
{
    public partial class oknoRezerwacjiSamochodu : Form
    {
        private string _login;
        private int _idKlienta;
        string _connectionString = "Data Source=DESKTOP-MOGMF2U\\SQLEXPRESS;Initial Catalog=WypozyczalniaSamochodow;Integrated Security=True";
        public oknoRezerwacjiSamochodu(string login)
        {
            InitializeComponent();
            _login = login;

            var Baza = new WypozyczalniaSamochodowEntities3();
            foreach (var klient in Baza.KLIENCI)
            {
                if (klient.Login == _login)
                {
                    _idKlienta = klient.KlientID;
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string IdSamochodu = textBox1.Text;
            string OdKiedy = textBox2.Text;
            string DoKiedy = textBox3.Text;
            string NrSalonuZ = textBox4.Text;
            string NrSalonuDo = textBox5.Text;
 
            string polecenie = "INSERT INTO WYPOZYCZENIE ([KlientID], [SamochodID], [OdKiedy], [DoKiedy], [MiejsceWypozyczeniaID], [MiejsceOddaniaID], [Przebieg], [Uwagi], [CzyZrealizowano], [PracWypozycajacyID], [PracPrzyjmujacyID], [DoZaplaty], [CzyZaplacono]) VALUES('" + _idKlienta.ToString() + "', '" + IdSamochodu + "', '" + OdKiedy + "', '" + DoKiedy + "', '" + NrSalonuZ + "','" + NrSalonuDo + "','0', '0', '0', '0', '0', '0', '0')";
            
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand(polecenie, conn);
                try
                {
                    cmd.ExecuteScalar();
                    MessageBox.Show("Formularz wysłano poprawnie, wkrótce potwierdzimy rezerwację !");
                    this.Close();
                }
                catch (SqlException)
                {
                    MessageBox.Show("Niepoprawne dane!!!");
                }

                conn.Close();
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
