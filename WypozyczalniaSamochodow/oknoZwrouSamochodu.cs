﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WypozyczalniaSamochodow
{
    public partial class oknoZwrouSamochodu : Form
    {
        string _connectionString = "Data Source=DESKTOP-MOGMF2U\\SQLEXPRESS;Initial Catalog=WypozyczalniaSamochodow;Integrated Security=True";
        public oknoZwrouSamochodu()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
         
            Sprawdzacz sprawdz = new Sprawdzacz(int.Parse(textBox1.Text));


            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                try
                {
                    int wypID = int.Parse(textBox1.Text);
                    int pracPrzyj = int.Parse(textBox2.Text);
                    int doZaplaty = int.Parse(textBox4.Text);
                    int przebieg = int.Parse(textBox3.Text);
                    string polecenie1 = "UPDATE WYPOZYCZENIE SET PracPrzyjmujacyID = " + pracPrzyj + " , Uwagi = '" + textBox5.Text + "', CzyZaplacono = 1, CzyZrealizowano = 1, DoZaplaty = " + doZaplaty + ", Przebieg = " + przebieg + " WHERE WypozyczenieID =" + wypID;

                    conn.Open();
                    if (sprawdz.czyMoznaZwrocic())
                    {
                        SqlCommand cmd1 = new SqlCommand(polecenie1, conn);
                        cmd1.ExecuteScalar();
                        MessageBox.Show("Zwrocono samochód");
                        this.Close();
                    }

                    else
                        MessageBox.Show("Nie istnieje wypożyczenie o takim ID !!!");
                }
                catch(FormatException)
                {
                    MessageBox.Show("Błędna wartość !");
                }



                conn.Close();
            }
        }
    }
}
