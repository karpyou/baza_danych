﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
 
namespace WypozyczalniaSamochodow
{
    public partial class oknoPracownika : Form
    {
        private string _idKlienta;
        private string _connectionString = "Data Source=DESKTOP-MOGMF2U\\SQLEXPRESS;Initial Catalog=WypozyczalniaSamochodow;Integrated Security=True";
        public oknoPracownika()
        {
            InitializeComponent();

        }

        public oknoPracownika(string idKlienta)
        {
            InitializeComponent();
            _idKlienta = idKlienta;
        }

        private void buttonPokazKlientow_Click(object sender, EventArgs e)
        {
            string sql = "SELECT * FROM KLIENCI";
            SqlConnection connection = new SqlConnection(_connectionString);
            SqlDataAdapter dataadapter = new SqlDataAdapter(sql, connection);
            DataSet ds = new DataSet();
            connection.Open();
            dataadapter.Fill(ds, "KLIENCI");
            connection.Close();
            wyswietlaczDanych.DataSource = ds;
            wyswietlaczDanych.DataMember = "KLIENCI";


        }

        private void buttonDodajKlienta_Click(object sender, EventArgs e)
        {
            oknoDodajKlienta okienko = new oknoDodajKlienta();
            okienko.Show();
        }

        private void buttonUsunKlienta_Click(object sender, EventArgs e)
        {
            oknoUsunKlienta okno = new oknoUsunKlienta();
            okno.Show();
        }

        private void buttonZmodyfikujKlienta_Click(object sender, EventArgs e)
        {
            oknoModyfikacjiKlienta okno = new oknoModyfikacjiKlienta();
            okno.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            oknoPytaniaID okienko = new oknoPytaniaID();
            okienko.Show();
            this.Close();
        }

        public void wyswietl_wyp()
        {
            this.Show();
            string sql = "SELECT Imie, Nazwisko, SamochodID, OdKiedy, DoKiedy, MiejsceWypozyczeniaID, MiejsceOddaniaID, Przebieg, Uwagi, CzyZrealizowano, PracWypozycajacyID, PracPrzyjmujacyID, DoZaplaty, CzyZaplacono, CzyZrealizowano FROM WYPOZYCZENIE e JOIN KLIENCI d ON e.KlientID = d.KlientID WHERE d.Nazwisko LIKE '" + _idKlienta + "'";

            SqlConnection connection = new SqlConnection(_connectionString);
            SqlDataAdapter dataadapter = new SqlDataAdapter(sql, connection);
            DataSet ds = new DataSet();
            connection.Open();
            dataadapter.Fill(ds, "WYPOZYCZENIE");
            connection.Close();
            wyswietlaczDanych.DataSource = ds;
            wyswietlaczDanych.DataMember = "WYPOZYCZENIE";

        }

        private void button6_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string sql= "SELECT WypozyczenieID, KlientID, SamochodID, OdKiedy, DoKiedy, MiejsceWypozyczeniaID, MiejsceOddaniaID FROM WYPOZYCZENIE WHERE CzyZrealizowano = 0";


            SqlConnection connection = new SqlConnection(_connectionString);
            SqlDataAdapter dataadapter = new SqlDataAdapter(sql, connection);
            DataSet ds = new DataSet();
            connection.Open();
            dataadapter.Fill(ds, "WYPOZYCZENIE");
            connection.Close();
            wyswietlaczDanych.DataSource = ds;
            wyswietlaczDanych.DataMember = "WYPOZYCZENIE";
        }

        private void button4_Click(object sender, EventArgs e)
        {
            oknoWydaniaSamochodu okienko = new oknoWydaniaSamochodu();
            okienko.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            oknoZwrouSamochodu okienko = new oknoZwrouSamochodu();
            okienko.Show();
        }
    }
}
