﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Windows.Forms;
using System.Reflection;

namespace WypozyczalniaSamochodow
{
    public partial class oknoLogowania : Form
    {
        public string connectionString;
        public string sql;

        public oknoLogowania()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var logowacz = new Sprawdzacz(boxLogin.Text, boxHaslo.Text, boxtypUzytkownika.Text);
            //if (!logowacz.zaloguj())
            if(false)
                MessageBox.Show("Niepoprawne logowanie");
            else
            {
                if (boxtypUzytkownika.Text=="Pracownik")
                {
                    oknoPracownika oknoPrac = new oknoPracownika();
                    oknoPrac.Show();
                }

                if (boxtypUzytkownika.Text=="Klient")
                {
                    oknoKlienta oknoKlient = new oknoKlienta(boxLogin.Text);
                    oknoKlient.Show();
                }


            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
