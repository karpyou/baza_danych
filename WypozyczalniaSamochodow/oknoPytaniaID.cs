﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WypozyczalniaSamochodow
{
    public partial class oknoPytaniaID : Form
    {
        string _connectionString = "Data Source=DESKTOP-MOGMF2U\\SQLEXPRESS;Initial Catalog=WypozyczalniaSamochodow;Integrated Security=True";
        public oknoPytaniaID()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string idKlienta1 = textBox1.Text;

            using (SqlConnection conn = new SqlConnection(_connectionString))
            {

                conn.Open();

                oknoPracownika okienko = new oknoPracownika(idKlienta1);
                okienko.wyswietl_wyp();
                this.Close();

                conn.Close();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            oknoPracownika okienko = new oknoPracownika();
            okienko.Show();
            this.Close();
        }
    }
}
