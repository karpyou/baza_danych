﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data.SqlTypes;

namespace WypozyczalniaSamochodow
{
    public class Sprawdzacz
    {
        private string _login { get; set; }
        private string _haslo { get; set; }
        private string _uzytkownik { get; set; }
        private int _idKlienta { get; set; }
        private string _connectionString = "Data Source=DESKTOP-MOGMF2U\\SQLEXPRESS;Initial Catalog=WypozyczalniaSamochodow;Integrated Security=True";

        public Sprawdzacz(string login, string haslo, string uzytkownik)
        {
            _login = login;
            _haslo = haslo;
            _uzytkownik = uzytkownik;
        }

        public Sprawdzacz(int idKlienta)
        {
            _idKlienta = idKlienta;
        }

        public bool zaloguj()
        {
            string uzytkownik;
            if (_uzytkownik == "Klient") uzytkownik = "KLIENCI";
            else uzytkownik = "PRACOWNICY";

            string polecenie = "SELECT COUNT (*) FROM " + uzytkownik + " WHERE Login = '" + _login + "' AND Haslo ='" + _haslo + "'";
            int blee;
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(polecenie, conn);
                blee = (int)cmd.ExecuteScalar();
                conn.Close();

                if (blee == 0)
                    return false;
                else
                    return true;
            }
        }

        public bool usunKlienta()
        {
            string polecenie = "SELECT COUNT (*) FROM KLIENCI WHERE KlientID = " + _idKlienta;
            int blee;
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(polecenie, conn);
                blee = (int)cmd.ExecuteScalar();
                conn.Close();

                if (blee == 0)
                    return false;
                else
                    return true;
            }
        }

        public bool czyIstniejeWyp()
        {
            string polecenie = "SELECT COUNT (*) FROM WYPOZYCZENIE WHERE WypozyczenieID= " + _idKlienta + "and CzyZrealizowano=0";
            int blee;
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(polecenie, conn);
                blee = (int)cmd.ExecuteScalar();
                conn.Close();

                if (blee == 0)
                    return false;
                else
                    return true;
            }
        }

        public bool czyMoznaZwrocic()
        {
            string polecenie = "SELECT COUNT (*) FROM WYPOZYCZENIE WHERE WypozyczenieID= " + _idKlienta + "and PracWypozycajacyID!=0 and CzyZrealizowano=0";
            int blee;
            using (SqlConnection conn = new SqlConnection(_connectionString))
            {
                conn.Open();
                SqlCommand cmd = new SqlCommand(polecenie, conn);
                blee = (int)cmd.ExecuteScalar();
                conn.Close();

                if (blee == 0)
                    return false;
                else
                    return true;
            }
        }

    }
}
